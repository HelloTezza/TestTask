package app;

import org.ini4j.Ini;
import org.ini4j.Profile;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class IplirReader {

    public String getSectionTextById(String id, String path) {
        Ini ini = new Ini();
        org.ini4j.Config iniConfig = ini.getConfig();
        iniConfig.setMultiSection(true);
        iniConfig.setMultiOption(true);

        ini.setConfig(iniConfig);
        File iplirConfFile = new File(path);
        try {
            ini = new Ini(iplirConfFile);
        } catch (IOException e) {
            throw new RuntimeException("Problems with file reading\n" + e.getMessage());
        }

        String sectionText = "";
        List<Profile.Section> idSections = ini.getAll("id");

        for (Profile.Section section : idSections) {
            if (section.get("id").equalsIgnoreCase(id)) {
                if (!sectionText.isEmpty())
                    throw new RuntimeException(String.format("There is more than one section with id='%s'", id));
                sectionText = section.toString();
            }
        }

        return sectionText;
    }

}
