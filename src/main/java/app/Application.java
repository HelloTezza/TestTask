package app;

import model.Config;

import java.io.IOException;
import java.util.List;

public class Application {

    public static void main(String[] args) {
        if (args.length == 0) throw new RuntimeException("There is no parameter with path to conf.txt file");

        ConfigReader configReader = new ConfigReader();
        Config config;
        String filePath = args[0];
        System.out.println("\nStart reading config...\n");
        try {
            config = configReader.readConfig(filePath);
        } catch (IOException e) {
            throw new RuntimeException("Problems with file reading\n" + e.getMessage());
        }
        System.out.println("Config successfully read\n");

        String configId = config.getId();
        String configPath = config.getPath();
        System.out.println(String.format("Search for section with id %s in file '%s' ... \n", configId, configPath));

        IplirReader iplirReader = new IplirReader();
        String sectionText = iplirReader.getSectionTextById(configId, configPath);

        if (sectionText.isEmpty())
            throw new RuntimeException(String.format("There is no section with id = '%s' in file %s", configId, configPath));

        List<String> regexps = config.getStrings();
        for (String regexp : regexps) {
            boolean matches = RegExpHelper.matches(sectionText, regexp);
            if (matches)
                System.out.println(String.format("Section matches to regexp = '%s'", regexp));
            else
                System.out.println(String.format("Section didn't matches to regexp = '%s'", regexp));
        }

    }

}
