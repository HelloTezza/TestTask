package app;

import model.Config;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ConfigReader {

    public Config readConfig(String filePath) throws IOException {
        List<String> stringsFromFile = getStringsFromFile(filePath);

        return createConfig(stringsFromFile);
    }

    private List<String> getStringsFromFile(String filePath) throws IOException {
        File file = new File(filePath);
        BufferedReader bufferedReader = new BufferedReader(new FileReader(file));

        List<String> fileLines = new ArrayList<>();

        String line = bufferedReader.readLine();

        while (line != null) {
            fileLines.add(line);
            line = bufferedReader.readLine();
        }

        return fileLines;
    }

    private Config createConfig(List<String> fileStrings) {
        Config config = new Config();

        List<String> pathValues = getValuesListByName(fileStrings, "path");
        if (pathValues.size() != 1)
         throw new IllegalArgumentException("Config file contains more than one 'path' parameter or didn't contains it at all");
        config.setPath(pathValues.get(0));

        List<String> idValues = getValuesListByName(fileStrings, "id");
        if (idValues.size() != 1)
            throw new IllegalArgumentException("Config file contains more than one 'id' parameter or didn't contains it at all");
        config.setId(idValues.get(0));

        List<String> stringValues = getValuesListByName(fileStrings, "string");
        config.setStrings(removeQuotes(stringValues));

        return config;
    }

    private List<String> getValuesListByName(List<String> fileStrings, String parameterName) {
        List<String> values = new ArrayList<>();

        for (String string : fileStrings) {
            if (string.split("=")[0].contains(parameterName)) {
                int valueStartIndex = string.indexOf("= ") + 2;
                String value = string.substring(valueStartIndex);
                values.add(value);
            }
        }

        return values;
    }

    private List<String> removeQuotes(List<String> strings) {
        List<String> cleanedStrings = new ArrayList<>();
        for (String string : strings) {
            string = string.substring(1);
            string = string.substring(0, string.length() - 1);
            cleanedStrings.add(string);
        }
        return cleanedStrings;
    }
}
